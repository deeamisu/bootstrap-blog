<?php

    include '../head.php';
    userIsOn();

    //Extragem din baza de date comentariile
    $comments=$_SESSION['comments'];

    //Extreagem din array numele coloanelor
    $header=$comments[0];
    ?>

    <!-- Meniu, submeniu -->
    <div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="addArticle.php">Adauga un articol</a></li>
            <li class="breadcrumb-item"><a href="editArticle.php">Editeaza un articol</a></li>
            <li class="breadcrumb-item active" aria-current="page">Comentarii</li>
            <li class="breadcrumb-item"><a href="logOut.php">Log Out</a></li>
        </ol>
    </nav>

    <!-- Meniu, submeniu -->

        <div class="row">
    <table border="1" cellspacing="0" cellpadding="0"><tr>

    <?php

    //Afisam header-ul

    foreach ($header as $key=>$value):
        ?><th><?php
            echo $key;
        ?></th><?php
    endforeach;
    ?>
            <th>Comanda1</th>
            <th>Comanda2</th>
            <th>Comanda3</th>
        </tr>

    <!--Afisam toate comentariile, cu datele aferente-->
            <?php
                foreach ($comments as $key=>$row):
                    ?><tr><?php
                    foreach ($row as $value)
                    {
                        ?><td style="text-align: center" width="200"><?php
                        echo $value;
                        ?></td><?php
                    }
                    ?>
                    <!--Inserare comenzi-->
                    <td><center><a href="accept.php?id=<?php echo $row['id'];?>">Accept</a></td></center>
                    <td><center><a href="edit.php?id=<?php echo $row['id'];?>">Editare</a></td></center>
                    <td><center><a href="delete.php?id=<?php echo $row['id'];?>">Sterge</a></td></center>

                    </tr>
                    <?php
                endforeach;
            ?>
    </table>
        </div>

    </div>
<?php

?>