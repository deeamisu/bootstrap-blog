<?php
include '../head.php';
userIsOn();
?>

<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Adauga un articol</li>
                    <li class="breadcrumb-item"><a href="editArticle.php">Editeaza un articol</a></li>
                    <li class="breadcrumb-item"><a href="panel.php">Comentarii</a></li>
                    <li class="breadcrumb-item"><a href="logOut.php">Log Out</a></li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <?php
            if (isset($_GET['error'])):
                ?> <div class="container m-4"> <?php
                echo "Va rugam sa completati cu date toate campurile!!";
                ?> </div> <?php
            endif;
            ?>
<form action="addArticleProcess.php" method="post">
    <div class="form-group">
        <label for="exampleFormControlInput1">Titlu:</label>
        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Titlul aritcolului" name="title">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput2">Autor:</label>
        <input type="text" class="form-control" id="exampleFormControlInput2" placeholder="Numele si prenumele autorului" name="author">
    </div>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Categorie:</label>
        <select class="form-control" id="exampleFormControlSelect1" name="category">
            <option value="STANDARD">STANDARD</option>
            <option value="LATINO">LATINO</option>
            <option value="FITNESS">FITNESS</option>
            <option value="DIVERSE">DIVERSE</option>
        </select>
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Continut articol:</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="10" name="text"></textarea>
    </div>

    <button type="submit" class="btn btn-outline-primary btn-lg">Posteaza</button>
</form>
        </div>
    </div>
</div>

</body>
</html>