<?php

include '../head.php';
userIsOn();
$articles=querryString("SELECT * FROM article WHERE id=".intval($_GET['id']));

?>

<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <form action="editArticleProcess.php?id=<?php echo $articles[0]['id'];?>" method="post">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Titlu:</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" value="<?php echo $articles[0]['title'];?>" name="title">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput2">Autor:</label>
                    <input type="text" class="form-control" id="exampleFormControlInput2"  name="author" value="<?php echo $articles[0]['author'];?>">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Categorie:</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="category">
                        <option value="STANDARD" <?php echo isSelected('STANDARD',$articles[0]['category']);?>>STANDARD</option>
                        <option value="LATINO" <?php echo isSelected('LATINO',$articles[0]['category']);?>>LATINO</option>
                        <option value="FITNESS" <?php echo isSelected('FITNESS',$articles[0]['category']);?>>FITNESS</option>
                        <option value="DIVERSE" <?php echo isSelected('DIVERSE',$articles[0]['category']);?>>DIVERSE</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Continut articol:</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="10" name="text">
                        <?php echo $articles[0]['text'];?></textarea>
                </div>

                <button type="submit" class="btn btn-outline-primary btn-lg">Actualizeaza</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12 m-4"><a style="margin-left: 20%;" href="editArticleProcess.php?delete=<?php echo $articles[0]['id'];?>"><b><i>Sterge Articol</i></b></a></div>
    </div>
    <div class="row">
        <div class="col-12 m-4"><a style="margin-left: 20%;" href="articleAdmin.php"><b><i>Administrare Articol</i></b></a></div>
    </div>
</div>


</body>
</html>