<?php
include "../head.php";
userIsOn();
?>
<body>
    <div class="container">
        <div class="row m-5">
            <div class="col-sm-2 col-lg-4"></div>
            <div class="col-sm-10 col-lg-8">
                <a href="addArticle.php">Adauga un articol</a>
            </div>
        </div>
        <div class="row m-5">
            <div class="col-sm-2 col-lg-4"></div>
            <div class="col-sm-10 col-lg-8">
                <a href="editArticle.php">Editeaza un articol</a>
            </div>
        </div>
        <div class="row m-5">
            <div class="col-sm-2 col-lg-4"></div>
            <div class="col-sm-10 col-lg-8">
                <a href="panel.php">Comentarii</a>
            </div>
        </div>
        <div class="row m-5">
            <div class="col-sm-2 col-lg-4"></div>
            <div class="col-sm-10 col-lg-8">
                <a href="logOut.php">Log Out</a>
            </div>
        </div>
    </div>

</body>
</html>