<?php include '../head.php';?>
<body>
    <div class="container">
        <div class="row">
            <div class="d-none d-lg-block col-lg-4"></div>
            <div class="col-sm-12 col-lg-4">
                <form class="card p-4" style="margin-top: 30%" method="post" action="logInProcess.php">
                    <div class="form-group">
                        <label for="exampleDropdownFormEmail2">Nume utilizator:</label>
                        <input type="text" class="form-control" id="exampleDropdownFormEmail2" name="user">
                    </div>
                    <div class="form-group">
                        <label for="exampleDropdownFormPassword2">Parola:</label>
                        <input type="password" class="form-control" id="exampleDropdownFormPassword2" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary">Autentificare</button>
                </form>
            </div>
            <div class="d-none d-lg-block col-lg-4"></div>
        </div>
    </div>
</body>
</html>