<?php

include '../head.php';
userIsOn();

$articles=querryString("SELECT * FROM article");

?>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="addArticle.php">Adauga un articol</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Editeaza un articol</li>
                        <li class="breadcrumb-item"><a href="panel.php">Comentarii</a></li>
                        <li class="breadcrumb-item"><a href="logOut.php">Log Out</a></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row m-lg-5 p-lg-5">
            <div class="d-none d-lg-3"></div>
            <div class="col-sm-12 col-lg-6">

                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Titlu</th>
                        <th scope="col">Autor</th>
                        <th scope="col">Categorie</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach ($articles as $item):
                            ?>
                            <tr>
                                <td>
                                    <?php echo "<a href='editArticleContent.php?id=".$item['id']."'>".$item['title']."</a>";?>
                                </td>
                                <td>
                                    <?php echo $item['author'];?>
                                </td>
                                <td>
                                    <?php echo $item['category'];?>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    ?>
                    </tbody>
                </table>

            </div>
            <div class="d-none d-lg-3"></div>
        </div>
    </div>
</body>
</html>
