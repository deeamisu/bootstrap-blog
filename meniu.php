
<!--Meniu-->

<div class="container menu">
    <nav class="navbar navbar-expand-lg navbar-light w-auto p-3">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="home.php?cat=*"><h2 class="menu">Home</h2><span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <h2 class="menu">Categorii</h2>
                    </a>
                    <div class="dropdown-menu p-3 mb-2 bg-transparent text-dark" style="border: none" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="home.php?cat=STANDARD"><h2 class="menu">Standard</h2></a>
                        <a class="dropdown-item" href="home.php?cat=LATINO"><h2 class="menu">Latino</h2></a>
                        <a class="dropdown-item" href="home.php?cat=FITNESS"><h2 class="menu">Fitness</h2></a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="home.php?cat=DIVERSE"><h2 class="menu">Diverse</h2></a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0" method="post">
                <input class="form-control mr-sm-2" type="search" placeholder="Cauta" name="search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
</div>