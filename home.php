
<?php include 'head.php';?>

<body>

<!--Background-->
<div class="mainBackground" id="wrapper">
    <div class="content">

            <!--Body-->
            <center>
            <div style="width: 90%" id="body">

                <?php
                include 'header.php';
                include 'meniu.php';
                include 'continut.php';
                include 'footer.php';
                ?>

            </div>
            </center>
    </div>
</div>

</body>
</html>